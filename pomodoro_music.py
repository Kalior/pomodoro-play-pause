import time
from typing import Final, Optional, Callable
import subprocess
import sys
from enum import Enum

from blessed import Terminal

term = Terminal()

TWENTY_FIVE_MINUTES: Final[int] = 60 * 25
FIVE_MINUTES: Final[int] = 60 * 5
FIFTEEN_MINUTES: Final[int] = 60 * 15


class State(Enum):
    Work = "w"
    Break = "b"
    LongBreak = "l"


def _get_next_action(state: State) -> Callable[[], State]:
    if state == State.Break:
        return _short_break
    elif state == State.Work:
        return _work
    elif state == State.LongBreak:
        return _long_break


def pomodoro():
    state = State.Work
    while True:
        state = _get_next_action(state)()


def _work() -> State:
    _play_music()
    new_state = _loop(TWENTY_FIVE_MINUTES, "Working for")

    if new_state is not None:
        return new_state
    else:
        return State.Break


def _short_break() -> State:
    _pause_music()
    new_state = _loop(FIVE_MINUTES, "Short break")
    if new_state is not None:
        return new_state
    else:
        return State.Work


def _long_break() -> State:
    _pause_music()
    new_state = _loop(FIFTEEN_MINUTES, "Long break")
    if new_state is not None:
        return new_state
    else:
        return State.Work


def _loop(duration: int, type_: str) -> Optional[State]:
    end_time = time.perf_counter() + duration
    now_time = time.perf_counter()
    while end_time > now_time:
        minutes_left = int((end_time - now_time) // 60)
        secons_left = int((end_time - now_time) % 60)
        output = f"{type_} {minutes_left:02d}:{secons_left:02d} more minutes"

        print(term.move_yx(1, 0) + output)

        inp = term.inkey(timeout=1)
        if inp == "q":
            sys.exit(0)
        elif inp in ["b", "l", "w"]:
            return State(inp)

        now_time = time.perf_counter()

    return None


def _pause_music():
    subprocess.run(("playerctl", "pause"))


def _play_music():
    subprocess.run(("playerctl", "play"))


if __name__ == "__main__":
    info_line = (
        f'type {term.bold("q")} to exit, '
        f'{term.bold("b")} to break, '
        f'{term.bold("l")} to long break and '
        f'{term.bold("w")} for work'
    )

    with term.fullscreen(), term.cbreak():
        print(term.move_yx(0, 0) + info_line)
        pomodoro()
